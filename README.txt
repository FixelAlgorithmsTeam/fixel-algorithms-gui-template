How to include PS files:
1. Open the project (Win\FaDummyIT.sln file)
2. Open Property Manager
   right click on PropertySheet,
   and then hit Properties:
   set PS_Include to the correct SDK folder location:
3. set Linker Output file location.

The code flow should be very simple to understand.
you should start from the function: FaDummyIT in the FaDummyIT_PS.cpp file.
