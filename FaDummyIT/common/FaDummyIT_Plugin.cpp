// ADOBE SYSTEMS INCORPORATED
// Copyright  1993 - 2002 Adobe Systems Incorporated
// All Rights Reserved
//
// NOTICE:  Adobe permits you to use, modify, and distribute this
// file in accordance with the terms of the Adobe license agreement
// accompanying it.  If you have received this file from a source
// other than Adobe, then your use, modification, or distribution
// of it requires the prior written permission of Adobe.
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//
//	File:
//		FaDummyIT.cpp
//
//	Description:
//		This file contains the source and routines for the
//		Filter module Poor Man's Type Tool, a module that
//		uses the Channel Ports Suite for pixel munging.
//
//	Use:
//		This is a basic module to exemplify all the typical
//		functions a filter module will do: Read scripting
//		parameters, show a user interface, display a proxy,
//		write changed pixel information, and then write
//		scripting parameters.
//
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//	Includes
//-------------------------------------------------------------------------------

#include "FilterBigDocument.h"

#include "FaDummyIT_Plugin.h"
#include "FaDummyIT_PS.h"
#include "Filter_Config.h"

//-------------------------------------------------------------------------------
//	Prototypes.
//-------------------------------------------------------------------------------

void InitGlobals       ( Ptr globalPtr );	// Initialize globals.
void ValidateParameters( GPtr globals );	// Validates (inits) parameters.

void DoParameters( GPtr globals );
void DoPrepare   ( GPtr globals );
void DoStart     ( GPtr globals );			// Main routine.
void DoContinue  ();
void DoFinish    ( GPtr globals );

void scaleRect ( Rect *l, short n, short d );
void shrinkRect( Rect *l, short x, short y );
void copyRect  ( Rect *l, const VRect *r );
short NumberOfChannels( GPtr globals );
void MergeSelectionAndMaskIntoMask(GPtr globals);

//-------------------------------------------------------------------------------
//	Globals -- Define global variables for plug-in scope.
//-------------------------------------------------------------------------------

FilterRecord * gFilterRecord = NULL;
SPBasicSuite * sSPBasic = NULL;

//-------------------------------------------------------------------------------
//
//	PluginMain / main
//
//	All calls to the plug-in module come through this routine.
//	It must be placed first in the resource.  To achieve this,
//	most development systems require this be the first routine
//	in the source.
//
//	The entrypoint will be "pascal void" for Macintosh,
//	"void" for Windows.
//
//	Inputs:
//		const int16 selector				Host provides selector indicating
//											what command to do.
//
//		FilterRecord *filterParamBlock		Host provides pointer to parameter
//											block containing pertinent data
//											and callbacks from the host.
//											See PIFilter.h.
//
//	Outputs:
//		FilterRecord *filterParamBlock		Host provides pointer to parameter
//											block containing pertinent data
//											and callbacks from the host.
//											See PIFilter.h.
//
//		void *data							Use this to store a handle or pointer to our
//											global parameters structure, which
//											is maintained by the host between
//											calls to the plug-in.
//
//		int16 *result						Return error result or noErr.  Some
//											errors are handled by the host, some
//											are silent, and some you must handle.
//											See PIGeneral.h.
//
//-------------------------------------------------------------------------------

DLLExport MACPASCAL void PluginMain (const int16 selector,
						             FilterRecordPtr filterParamBlock,
						             intptr_t *data,
						             int16 *result)
{

try {

	//---------------------------------------------------------------------------
	//	(1) Check for about box request.
	//
	// 	The about box is a special request; the parameter block is not filled
	// 	out, none of the callbacks or standard data is available.  Instead,
	// 	the parameter block points to an AboutRecord, which is used
	// 	on Windows.
	//---------------------------------------------------------------------------

	gFilterRecord = filterParamBlock;

	if (selector == filterSelectorAbout)
	{
		sSPBasic = ((AboutRecordPtr)filterParamBlock)->sSPBasic;
		//DoAbout((AboutRecordPtr)filterParamBlock);
	}
	else
	{ // do the rest of the process as normal:

		sSPBasic = ((FilterRecordPtr)filterParamBlock)->sSPBasic;

		Ptr globalPtr = NULL;		// Pointer for global structure
		GPtr globals = NULL; 		// actual globals

		//-----------------------------------------------------------------------
		//	(2) Allocate and initalize globals.
		//
		// 	AllocateGlobals requires the pointer to result, the pointer to the
		// 	parameter block, a pointer to the handle procs, the size of our local
		// 	"Globals" structure, a pointer to the long *data, a Function
		// 	Proc (FProcP) to the InitGlobals routine.  It automatically sets-up,
		// 	initializes the globals (if necessary), results result to 0, and
		// 	returns with a valid pointer to the locked globals handle or NULL.
		//-----------------------------------------------------------------------

		globalPtr = AllocateGlobals (result,
									 filterParamBlock,
									 filterParamBlock->handleProcs,
									 sizeof(Globals),
						 			 data,
						 			 InitGlobals);
		if (globalPtr == NULL)
		{ // Something bad happened if we couldn't allocate our pointer.
		  // Fortunately, everything's already been cleaned up,
		  // so all we have to do is report an error.

		  *result = memFullErr;
		  return;
		}

		// Get our "globals" variable assigned as a Global Pointer struct with the
		// data we've returned:
		globals = (GPtr)globalPtr;

		// See if our suite pointers are initialized. If you run this plug in on
		// Photoshop 5.0.2 you will get an error the first time you run because
		// InitGlobals() fails. The second time you run InitGlobals() does not get
		// called.
		if (gPSChannelPortsSuite == NULL || gPSBufferSuite64 == NULL)
		{
			*result = errPlugInHostInsufficient;
			return;
		}

		//-----------------------------------------------------------------------
		//	(3) Dispatch selector.
		//-----------------------------------------------------------------------

		switch (selector)
		{
			case filterSelectorParameters:
				DoParameters(globals);
				break;
			case filterSelectorPrepare:
				DoPrepare(globals);
				break;
			case filterSelectorStart:
				DoStart(globals);
				break;
			case filterSelectorContinue:
				DoContinue();
				break;
			case filterSelectorFinish:
				DoFinish(globals);
				break;
		}

		//-----------------------------------------------------------------------
		//	(4) Unlock data, and exit resource.
		//
		//	Result is automatically returned in *result, which is
		//	pointed to by gResult.
		//-----------------------------------------------------------------------

		// unlock handle pointing to parameter block and data so it can move
		// if memory gets shuffled:
		if ((Handle)*data != NULL)
			PIUnlockHandle((Handle)*data);

	} // about selector special

	} // end try

	catch (...)
	{
		if (NULL != result)
			*result = -1;
	}

} // end PluginMain

//-------------------------------------------------------------------------------
//
//	InitGlobals
//
//	Initalize any global values here.  Called only once when global
//	space is reserved for the first time.
//
//	Inputs:
//		Ptr globalPtr		Standard pointer to a global structure.
//
//	Outputs:
//		Initializes any global values with their defaults.
//
//-------------------------------------------------------------------------------

void InitGlobals (Ptr globalPtr)
{
	// create "globals" as a our struct global pointer so that any
	// macros work:
	GPtr globals = (GPtr)globalPtr;

	gDocDesc = gDocInfo;
	gChannelData = NULL;
	gMaskData = NULL;
	gSelectionData = NULL;
	gOverlayData = NULL;
	if (gStuff->sSPBasic->AcquireSuite( kPSChannelPortsSuite,
					    				kPSChannelPortsSuiteVersion3,
										(const void **)&gPSChannelPortsSuite))
			gResult = errPlugInHostInsufficient;
	if (gStuff->sSPBasic->AcquireSuite(	kPSBufferSuite,
										kPSBufferSuiteVersion2,
										(const void **)&gPSBufferSuite64))
			gResult = errPlugInHostInsufficient;

	// Initialize global variables:
	ValidateParameters (globals);
} // end InitGlobals


//-------------------------------------------------------------------------------
//
//	ValidateParameters
//
//	Initialize parameters to default values.
//
//	Inputs:
//		GPtr globals		Pointer to global structure.
//
//	Outputs:
//		gPointH				Default: 0.
//
//		gPointV				Default: 0.
//
//		gXFactor			Default: 4.
//
//		gIgnoreSelection	Default: false.
//
//-------------------------------------------------------------------------------

void ValidateParameters (GPtr globals)
{
	if (gStuff->parameters == NULL)
	{ // We haven't created these yet.

		gStuff->parameters = PINewHandle ((long) sizeof (TParameters));

		if (gStuff->parameters != NULL)
		{ // Got it.  Fill out the fields.
			gScale1  = 1;
			gScale2  = 1;
			gScale3  = 1;
			gScale4  = 1;
			gScale5  = 1;
			gLumiModeFlag	= true;
			gIntensityInput = 75;
		}
		else
		{ // Oops.  Couldn't allocate memory.

			gResult = memFullErr;
			return;
		}
	} // parameters

} // end ValidateParameters


//-------------------------------------------------------------------------------
//
//	DoParameters
//
//	Initialize parameters to default values.
//
//	Inputs:
//
//	Outputs:

/* Asks the user for the plug-in filter module's parameters. Note that
   the image size information is not yet defined at this point. Also, do
   not assume that the calling program will call this routine every time the
   filter is run (it may save the data held by the parameters handle in
   a macro file). Initialize any single-time info here. */

void DoParameters (GPtr globals)

{
	ValidateParameters (globals);

	gQueryForParameters = TRUE;
	/* If we're here, that means we're being called for the first time. */
}


/*****************************************************************************/

/* Prepare to filter an image.	If the plug-in filter needs a large amount
   of buffer memory, this routine should set the bufferSpace field to the
   number of bytes required.

   In this example we are using the Buffer Suite, this allows us to not have
   to deal with the bufferSpace field. We make all our requests for memory
   through the gPSBufferSuite->New() routine.

*/

void DoPrepare (GPtr globals)
{
	ValidateParameters (globals);
}


/*****************************************************************************/

void DoStart (GPtr globals)
{
	ValidateParameters (globals);
	/* if stuff hasn't been initialized that we need, do it,
		then go check if we've got scripting commands to
		override our settings */

	// update our parameters with the scripting parameters, if available
	ReadScriptParams (globals);

	// always reset the global document descriptor for safety
	gDocDesc = gDocInfo;

	// Delete the memory we used for the Proxy view
	ReleaseProxyMemory(globals);

	if (gResult != noErr)
		return;


	// Do the actual filtering operation on the original image

	try
	{
		VRect filterRect = GetFilterRect();
		SetInRect(filterRect);
		SetOutRect(filterRect);

		const Point imageSizePoint = gFilterRecord->imageSize;

		PS_Database sDatabase;

		int planes = gFilterRecord->planes;
		if (planes > 3)
			planes = 3;

		gFilterRecord->outLoPlane = gFilterRecord->inLoPlane = 0;
		gFilterRecord->outHiPlane = gFilterRecord->inHiPlane = planes - 1;
		gFilterRecord->advanceState();

		sDatabase.planes       = planes;
		sDatabase.pInputImage  = gFilterRecord->inData;
		sDatabase.pOutputImage = gFilterRecord->outData;
		sDatabase.inputStep    = gFilterRecord->inRowBytes;
		sDatabase.outputStep   = gFilterRecord->outRowBytes;
		sDatabase.height       = imageSizePoint.v;
		sDatabase.width        = imageSizePoint.h;

		switch (gFilterRecord->depth)
		{
		case 8:  sDatabase.format = PS_Database::FORMAT_8;  break;
		case 16: sDatabase.format = PS_Database::FORMAT_16; break;
		case 32: sDatabase.format = PS_Database::FORMAT_32; break;
		}

		FaDummyIT(sDatabase);
	}
	catch (NO_MEMORY_ERROR)
	{
		gResult = memFullErr;
		throw;
	}
}


void DoContinue(void)
{
	VRect zeroRect = { 0, 0, 0, 0 };

	SetInRect(zeroRect);
	SetOutRect(zeroRect);
	//SetMaskRect(zeroRect);
}


/*****************************************************************************/

/* Do any necessary clean-up. */

void DoFinish (GPtr globals)
{
	// writes our parameters to the scripting system
	WriteScriptParams (globals);
}

/***********************************************************************************/

/* Releases the local memory we used to display the proxy image view. */


void ReleaseProxyMemory(GPtr globals)
{
	if (gSelectionData != NULL)
	{
		gPSBufferSuite64->Dispose((char**)&gSelectionData);
		gSelectionData = NULL;
	}

	if (gMaskData != NULL)
	{
		gPSBufferSuite64->Dispose((char**)&gMaskData);
		gMaskData = NULL;
	}

	if (gChannelData != NULL)
	{
		gPSBufferSuite64->Dispose((char**)&gChannelData);
		gChannelData = NULL;
	}

	if (gOverlayData != NULL)
	{
		gPSBufferSuite64->Dispose((char**)&gOverlayData);
		gOverlayData = NULL;
	}
}

/****************************************************************************/
// end FaDummyIT.cpp
