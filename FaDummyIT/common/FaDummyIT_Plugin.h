// ADOBE SYSTEMS INCORPORATED
// Copyright  1993 - 2002 Adobe Systems Incorporated
// All Rights Reserved
//
// NOTICE:  Adobe permits you to use, modify, and distribute this
// file in accordance with the terms of the Adobe license agreement
// accompanying it.  If you have received this file from a source
// other than Adobe, then your use, modification, or distribution
// of it requires the prior written permission of Adobe.
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//
//	File:
//		FaDummyIT.h
//
//	Description:
//		This file contains the source and routines for the
//		Filter module Poor Man's Type Tool, a module that
//		uses the Channel Ports Suite for pixel munging.
//
//	Use:
//		This is a basic module to exemplify all the typical
//		functions a filter module will do: Read scripting
//		parameters, show a user interface, display a proxy,
//		write changed pixel information, and then write
//		scripting parameters.
//
//-------------------------------------------------------------------------------

#ifndef __FaDummyIT_H__		// Has this not been defined yet?
#define __FaDummyIT_H__		// Only include this once by predefining it.

#include "PIFilter.h"						// Filter Photoshop header file.
#include "PIUtilities.h"					// SDK Utility library.
#include "PIChannelPortsSuite.h"			// Channel Ports Suite Photoshop header file.
#include "PIChannelPortOperations.h"		// Supported imaging operations on channel ports
namespace std {
	using ::size_t;
}
#include "PIBufferSuite.h"					// Buffer Suite Photoshop header file.
#include "FaDummyITTerminology.h"	// Terminology for this plug-in.
#if Macintosh
	#include <string.h>
#endif



//-------------------------------------------------------------------------------
//	Definitions -- Dialog items
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//	Definitions -- Constants
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//	Globals -- structures
//-------------------------------------------------------------------------------

typedef struct TParameters
{ // This is our parameter structure that has an entry in the parameter block.
	Boolean					queryForParameters;

	/*----------FaDummyIT-Params---------------------*/
	real64   scale1;
	real64   scale2;
	real64   scale3;
	real64   scale4;
	real64   scale5;
	Boolean  lumiModeFlag;
	real64   intensityInput;
	/*-----------------------------------------------*/

} TParameters, *PParameters, **HParameters;

typedef struct Globals
{ // This is our structure that we use to pass globals between routines:

	short					*result;				// Must always be first in Globals.
	FilterRecord			*filterParamBlock;		// Must always be second in Globals.

	ReadImageDocumentDesc	*docDesc;
	PSChannelPortsSuite1	*sPSChannelPortsSuite;
	PSBufferSuite2			*sPSBufferSuite64;
	char					*channelData;
	char					*maskData;
	char					*selectionData;
	char					*overlayData;

} Globals, *GPtr, **GHdl;

//-------------------------------------------------------------------------------
//	Globals -- definitions and macros
//-------------------------------------------------------------------------------

#define gResult 				(*(globals->result))
#define gStuff  				(globals->filterParamBlock)
#define gDocDesc				(globals->docDesc)
#define gPSChannelPortsSuite	(globals->sPSChannelPortsSuite)
#define gPSBufferSuite64		(globals->sPSBufferSuite64)
#define gChannelData			(globals->channelData)
#define gMaskData				(globals->maskData)
#define gSelectionData			(globals->selectionData)
#define gOverlayData			(globals->overlayData)

#define gParams 				((PParameters) *gStuff->parameters)
#define gQueryForParameters		(gParams->queryForParameters)

/*-----------------FaDummyIT-Params----------------------------*/
#define gScale1			        (gParams->scale1)
#define gScale2			        (gParams->scale2)
#define gScale3			        (gParams->scale3)
#define gScale4			        (gParams->scale4)
#define gScale5			        (gParams->scale5)
#define gLumiModeFlag			(gParams->lumiModeFlag)
#define gIntensityInput			(gParams->intensityInput)
/*----------------------------------------------------------*/
#define gDocInfo				(gStuff->documentInfo)


//-------------------------------------------------------------------------------
//	Prototypes
//-------------------------------------------------------------------------------

OSErr ReadScriptParams (GPtr globals);		// Read any scripting params.
OSErr WriteScriptParams (GPtr globals);		// Write any scripting params.


// release the memory held for the proxy item
void ReleaseProxyMemory(GPtr globals);

//-------------------------------------------------------------------------------

#endif // __FaDummyIT_H__

// end FaDummyIT.h
