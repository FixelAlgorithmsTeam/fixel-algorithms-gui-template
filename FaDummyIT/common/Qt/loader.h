#ifndef LOADER_H
#define LOADER_H

#include <QWidget>
#include <QMovie>

namespace Ui {
  class loader;
}

class loader : public QWidget
{
  Q_OBJECT

public:
  explicit loader(QWidget *parent = 0);
  ~loader();

  void start();
  void stop();

private:
  Ui::loader *ui;
  QMovie *m_pMovie;
};

#endif // LOADER_H
