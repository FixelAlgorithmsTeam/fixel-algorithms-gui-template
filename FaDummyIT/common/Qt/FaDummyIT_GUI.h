#ifndef FADUMMYIT_GUI_H
#define FADUMMYIT_GUI_H

#include <QtWidgets/QMainWindow>
#include "ui_FaDummyIT_GUI.h"

#include "FaDummyIT_Filter.h"
#include "FaImage.h"
#include "popup.h"

class FaDummyIT_GUI : public QMainWindow
{
	Q_OBJECT

public:
	Ui::FaDummyIT_GUIClass ui;

	popup*    pPopup;
	FaImage&  input_image;
	FaImage*  pOutput_image;
	QImage*   pQInput_image;
	QImage*   pQPreview_image;
	QPointF   qPoint;

	FaDummyAlgorithmParams* pAlgorithm_params;

	int image_width;
	int image_height;

	FaDummyIT_GUI(FaImage&, QWidget *parent = 0);
	~FaDummyIT_GUI();

	void Init           (FaDummyAlgorithmParams*);
	void Render         ();
	void Display_Preview();


public slots:
    void Radius_Silder_Changed(int radius_silder_value);
	void Silder2_Changed      (int silder2_value);
	void Check_Box_Changed    (int);
	void Drop_Box_Change      (int);
	void Apply_Button         ();
	void Cancel_Button        ();
	void About                ();

private:
};

#endif // FADUMMYIT_GUI_H
