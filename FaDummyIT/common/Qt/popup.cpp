#include "popup.h"
#include "ui_popup.h"
#include <QResizeEvent>

popup::popup(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::popup)
{
  ui->setupUi(this);
  connect(ui->close_button, SIGNAL(clicked()), this, SLOT(hide()));
}

popup::~popup() {
  delete ui;
}

void popup::setContentText(const QString &text) {
  ui->content_label->setText(text);
}
