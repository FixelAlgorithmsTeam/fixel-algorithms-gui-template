#ifndef _GRAPHICS_VIEW_GUI_H_
#define _GRAPHICS_VIEW_GUI_H_

#include <QtWidgets\QGraphicsview.h>
#include <QGraphicsItem>

/**********************************************************************************************************************/
/*****Full Resolution View*********************************************************************************************/
/**********************************************************************************************************************/

class Full_Resolution_View : public QGraphicsView
{
public:
	Full_Resolution_View(QWidget *parent);
	~Full_Resolution_View();

	void Init();

	QGraphicsScene*      pPreview_scene;
	QGraphicsPixmapItem* pFull_res_input_item;
	QGraphicsPixmapItem* pFull_res_preview_item;

	bool moving_in_progress;

protected:
    void mouseReleaseEvent(QMouseEvent *event);
	void mousePressEvent  (QMouseEvent *event);
	void mouseMoveEvent   (QMouseEvent *event);
	void wheelEvent       (QWheelEvent *event) {}


};

/**********************************************************************************************************************/
/*****Drag Rect Item***************************************************************************************************/
/**********************************************************************************************************************/

class Drag_Rect_Item : public QGraphicsRectItem
{
public:
	Drag_Rect_Item(const QRectF& rect, QGraphicsItem* parent = 0);

	QVariant itemChange(GraphicsItemChange change, const QVariant& value);
};


/**********************************************************************************************************************/
/*****Low Resolution View**********************************************************************************************/
/**********************************************************************************************************************/

class Low_Resolution_View : public QGraphicsView
{
public:
	Low_Resolution_View(QWidget *parent);
	~Low_Resolution_View();

	void Init();

	QGraphicsScene*      pLow_res_scene;
	QGraphicsPixmapItem* pLow_res_item;
	Drag_Rect_Item*      pRect_item;

	int                  view_width;
	int                  view_height;
	int                  image_width;
	int                  image_height;
	float                scale_factor;

protected:
    void mouseReleaseEvent(QMouseEvent *event);
};

#endif // _GRAPHICS_VIEW_GUI_H_
