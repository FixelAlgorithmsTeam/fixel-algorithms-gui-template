
#include "Graphics_View_GUI.h"
#include "FaDummyIT_GUI.h"
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QMouseEvent>

#include "FaImage.h"

extern FaDummyIT_GUI* pGUI;

/**********************************************************************************************************************/
/*****Full Resolution View*********************************************************************************************/
/**********************************************************************************************************************/

Full_Resolution_View::Full_Resolution_View(QWidget *parent):
    QGraphicsView         (parent),
	pFull_res_input_item  (new QGraphicsPixmapItem),
	pFull_res_preview_item(new QGraphicsPixmapItem),
	pPreview_scene        (new QGraphicsScene),
	moving_in_progress    (false)
{
}

Full_Resolution_View::~Full_Resolution_View()
{
	delete this->pFull_res_input_item;
	delete this->pFull_res_preview_item;
	delete this->pPreview_scene;
}

void Full_Resolution_View::Init()
{
	this->pFull_res_input_item->setPixmap(QPixmap::fromImage(*pGUI->pQInput_image));
	this->pPreview_scene->addItem(this->pFull_res_input_item);

	this->setScene(pPreview_scene);
	this->show();
}

void Full_Resolution_View::mousePressEvent(QMouseEvent *event)
{
	moving_in_progress = true;
	this->pPreview_scene->removeItem(this->pFull_res_preview_item);

	QGraphicsView::mousePressEvent(event);
}

void Full_Resolution_View::mouseMoveEvent(QMouseEvent *event)
{
	QPointF qPoint;

	qPoint = this->mapToScene(this->viewport()->pos());
	qPoint.rx()--; // to sync with the scene (there one pixel offset)
	qPoint.ry()--; // -""- ...

	pGUI->ui.low_res_view->pRect_item->setPos(qPoint);

	QGraphicsView::mouseMoveEvent(event);
}
void Full_Resolution_View::mouseReleaseEvent(QMouseEvent *event)

{
	moving_in_progress = false;

	pGUI->qPoint = this->mapToScene(this->viewport()->pos());
	pGUI->qPoint.rx()--; // to sync with the scene (there one pixel offset)
	pGUI->qPoint.ry()--; // -""- ...

	pGUI->ui.low_res_view->pRect_item->setPos(pGUI->qPoint);

	pGUI->Render();
	pGUI->Display_Preview();

	QGraphicsView::mouseReleaseEvent(event);
}

/**********************************************************************************************************************/
/*****Drag Rect Item***************************************************************************************************/
/**********************************************************************************************************************/

Drag_Rect_Item::Drag_Rect_Item(const QRectF & rect, QGraphicsItem * parent)
    :QGraphicsRectItem(rect, parent)
{
    this->setFlags(QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemSendsGeometryChanges);
}

QVariant Drag_Rect_Item::itemChange(GraphicsItemChange change, const QVariant & value)
{
	if (pGUI->ui.full_res_view->moving_in_progress == true || scene() && change != ItemPositionChange)
		return QGraphicsItem::itemChange(change, value);

	int rect_width  = pGUI->ui.full_res_view->width();
	int rect_height = pGUI->ui.full_res_view->height();

	bool    limit_flag = false;
	QPointF newPos  = value.toPointF();
	QRectF  limit_rect(1, 1, pGUI->input_image.Width() - rect_width, pGUI->input_image.Height() - rect_height);

	if (!limit_rect.contains(newPos))
	{
		// Keep the item inside the scene rect.
		newPos.setX( qMin(limit_rect.right(),  qMax( newPos.x(), limit_rect.left()) ) );
		newPos.setY( qMin(limit_rect.bottom(), qMax( newPos.y(), limit_rect.top())  ) );
		limit_flag = true;
	}

	QPointF view_point(newPos.x() + rect_height / 2, newPos.y() + rect_width / 2);
	pGUI->ui.full_res_view->centerOn(view_point);

	pGUI->qPoint = pGUI->ui.full_res_view->mapToScene(pGUI->ui.full_res_view->viewport()->pos());
	pGUI->qPoint.rx()--; // to sync with the scene (there one pixel offset)
	pGUI->qPoint.ry()--; // -""- ...

	pGUI->Render();
	pGUI->Display_Preview();

	if (change == ItemPositionChange && scene())
	{
		if (limit_flag == true)
			return newPos;
	}

	return QGraphicsItem::itemChange(change, value);
}

/**********************************************************************************************************************/
/*****Low Resolution View**********************************************************************************************/
/**********************************************************************************************************************/

Low_Resolution_View::Low_Resolution_View(QWidget *parent):
   QGraphicsView (parent),
   pLow_res_item (nullptr),
   pLow_res_scene(nullptr)
{
	pLow_res_item  = new QGraphicsPixmapItem;
	pLow_res_scene = new QGraphicsScene;
}

Low_Resolution_View::~Low_Resolution_View()
{
	delete this->pRect_item;
	delete this->pLow_res_item;
	delete this->pLow_res_scene;
}

void Low_Resolution_View::Init()
{

	if (pGUI->input_image.Width() > pGUI->input_image.Height())
	{
		this->view_width   = this->width() + 2;
		this->scale_factor = static_cast<float>(this->width()) / pGUI->input_image.Width();
		this->view_height  = ceil(pGUI->input_image.Height() * this->scale_factor) + 2;
	}
	else
	{
		this->view_height  = this->height() + 2;
		this->scale_factor = static_cast<float>(this->height()) / pGUI->input_image.Height();
		this->view_width   = ceil(pGUI->input_image.Width() * this->scale_factor) + 2;
	}

	this->pLow_res_item->setPixmap(QPixmap::fromImage(*pGUI->pQInput_image));
	this->pLow_res_scene->addItem(pLow_res_item);

	this->scale(this->scale_factor, this->scale_factor);

	QPen pen(QColor("#007FFF"));
	pen.setWidth(1);

	int rect_size = pGUI->ui.full_res_view->width();

	this->pRect_item = new Drag_Rect_Item(QRectF(1, 1, rect_size, rect_size));
	this->pRect_item->setPen(pen);
	this->pLow_res_scene->addItem(this->pRect_item);
	this->pRect_item->setPos(1, 1); // TODO: maybe the center..

	this->setScene(pLow_res_scene);
	this->show();
}


void Low_Resolution_View::mouseReleaseEvent(QMouseEvent *event)
{
	// TODO: fix offset of mouse point, happen because the black area in the view.
	QPointF mouse_point(event->pos() / this->scale_factor);
	QPointF rect_point = this->pRect_item->pos();

	int rect_width  = this->pRect_item->rect().width();
	int rect_height = this->pRect_item->rect().height();

	// Check if the click occur outside the rect
	if (mouse_point.x() < rect_point.x()              ||
		mouse_point.y() < rect_point.y()              ||
		mouse_point.x() > rect_point.x() + rect_width ||
		mouse_point.y() > rect_point.y() + rect_height)
	{
		this->pRect_item->setPos(mouse_point.x() - rect_width  / 2,
			                     mouse_point.y() - rect_height / 2);
	}

	QGraphicsView::mouseReleaseEvent(event);
}
