/********************************************************************************
** Form generated from reading UI file 'FaDummyIT_GUI.ui'
**
** Created by: Qt User Interface Compiler version 5.5.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FADUMMYIT_GUI_H
#define UI_FADUMMYIT_GUI_H

#include <Graphics_View_GUI.h>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDial>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>
#include "loader.h"

QT_BEGIN_NAMESPACE

class Ui_FaDummyIT_GUIClass
{
public:
    QAction *actionAbout;
    QWidget *centralWidget;
    Low_Resolution_View *low_res_view;
    QComboBox *comboBox;
    QSlider *slider_2;
    QSlider *radius_slider;
    QLabel *radius_slider_label;
    QSpinBox *slider2_spin_box;
    QCheckBox *checkBox;
    QSpinBox *radius_spin_box;
    QLabel *slider_2_label;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QPushButton *apply_button;
    QPushButton *cancel_button;
    QPushButton *about_button;
    QFrame *fullResFrame;
    Full_Resolution_View *full_res_view;
    loader *loader_widget;
    QDial *dial;

    void setupUi(QMainWindow *FaDummyIT_GUIClass)
    {
        if (FaDummyIT_GUIClass->objectName().isEmpty())
            FaDummyIT_GUIClass->setObjectName(QStringLiteral("FaDummyIT_GUIClass"));
        FaDummyIT_GUIClass->setWindowModality(Qt::NonModal);
        FaDummyIT_GUIClass->resize(1114, 704);
        FaDummyIT_GUIClass->setMinimumSize(QSize(1114, 704));
        FaDummyIT_GUIClass->setMaximumSize(QSize(1114, 704));
        QPalette palette;
        QBrush brush(QColor(0, 170, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(77, 77, 77, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        QBrush brush2(QColor(115, 115, 115, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Light, brush2);
        QBrush brush3(QColor(96, 96, 96, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        QBrush brush4(QColor(38, 38, 38, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush4);
        QBrush brush5(QColor(51, 51, 51, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        QBrush brush6(QColor(0, 0, 0, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush6);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush4);
        QBrush brush7(QColor(255, 255, 220, 255));
        brush7.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush7);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush7);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush6);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush6);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush7);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush6);
        FaDummyIT_GUIClass->setPalette(palette);
        FaDummyIT_GUIClass->setCursor(QCursor(Qt::ArrowCursor));
        FaDummyIT_GUIClass->setWindowOpacity(1);
        FaDummyIT_GUIClass->setStyleSheet(QLatin1String("#centralWidget {\n"
"	background-color: #171616;\n"
"}\n"
"\n"
"QSlider::groove:horizontal {\n"
"	height: 3px;\n"
"    background: \"transparent\";\n"
"    position: absolute; /* absolutely position 9px from the left and right of the widget. setting margins on the widget should work too... */\n"
"    left: 9px; right: 9px;\n"
"}\n"
"\n"
"QSlider::handle:horizontal {\n"
"    width: 18px;\n"
"   	background-image: url(:/ui/sliderHandle.png);\n"
"	background-position: center center;\n"
"	background-repeat: no-repeat;\n"
"    margin: -8px 0px; /* expand outside the groove */\n"
"}\n"
"\n"
"QSlider::add-page:horizontal {\n"
"    background: #444444;\n"
"	margin: 1px 1px;\n"
"}\n"
"\n"
"QSlider::sub-page:horizontal {\n"
"    background: #007FFF;\n"
"}\n"
"\n"
"QCheckBox {\n"
"	min-height: 20px;\n"
"	max-height: 20px;\n"
"	font-family: Lato;\n"
"	font-weight: bold;\n"
"	font-size: 12px;\n"
"	color: #798387;\n"
"}\n"
"\n"
"QCheckBox::indicator:unchecked {\n"
"    image: url(:/ui/checkboxUnchecked.png);\n"
"}\n"
"\n"
""
                        "QCheckBox::indicator:checked {\n"
"    image: url(:/ui/checkboxChecked.png);\n"
"}\n"
"\n"
"QPushButton {\n"
"	background-color: #222222;\n"
"	border-radius: 5px;\n"
"	border-style: none;\n"
"	min-height: 40px;\n"
"	max-height: 40px;\n"
"	min-width: 100px;\n"
"	font-family: Lato;\n"
"	font-weight: bold;\n"
"	font-size: 14px;\n"
"	color: #FFFFFF;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"	background-color: #333333;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"	background-color: #444444;\n"
"}\n"
"\n"
"QComboBox {\n"
"	border-radius: 5px;\n"
"	border-style: none;\n"
"	min-height: 36px;\n"
"	max-height: 36px;\n"
"	background-color: #222222;\n"
"	font-family: Lato;\n"
"	font-weight: bold;\n"
"	font-size: 12px;\n"
"	color: #FFFFFF;\n"
"	padding-left: 10px;\n"
"}\n"
"\n"
"QComboBox:hover {\n"
"	background-color: #333333;\n"
"}\n"
"\n"
"QComboBox::drop-down {\n"
"    subcontrol-origin: padding;\n"
"    subcontrol-position: top right;\n"
"    width: 36px;\n"
"	border-style: none;\n"
"	background-color: \"transparent\";\n"
"}"
                        "\n"
"\n"
"QComboBox::down-arrow {\n"
"    image: url(:/ui/dropDown.png);\n"
"}\n"
"\n"
"QComboBox::down-arrow:on { /* shift the arrow when popup is open */\n"
"    image: url(:/ui/dropUp.png);\n"
"}\n"
"\n"
"QComboBox:on {\n"
"	border-bottom-right-radius: 0px;\n"
"	border-bottom-left-radius: 0px;\n"
"	background-color: #444444;\n"
"}\n"
"\n"
"/*for combobox*/\n"
"QComboBox QAbstractItemView {\n"
"	font-family: Lato;\n"
"	font-weight: bold;\n"
"	font-size: 12px;\n"
"	border: none;\n"
"    background-color: #444444;\n"
"}\n"
"\n"
"/*for combobox*/\n"
"QListView {\n"
"	outline: none;\n"
"    border: none;\n"
"    background-color: #444444;\n"
"}\n"
"\n"
"/*for combobox*/\n"
"QListView::item {\n"
"	padding-left: 6px;\n"
"	min-height: 36px;\n"
"	max-height: 36px;\n"
"	border-style: none;\n"
"	background-color: #444444;\n"
"    font-family: Lato;\n"
"	font-weight: bold;\n"
"	font-size: 12px;\n"
"	color: #798387;\n"
"}\n"
"\n"
"/*for combobox*/\n"
"QListView::item:selected {\n"
"	color: #FFFFFF;\n"
"}\n"
"\n"
"QSpinB"
                        "ox {\n"
"    padding-right: 18px; /* make room for the arrows */\n"
"	min-height: 28px;\n"
"	max-height: 28px;\n"
"	border-style: none;\n"
"	background-color: \"transparent\";\n"
"	font-family: Lato;\n"
"	font-size: 24px;\n"
"	color: #7A8388;\n"
"	background-image: url(:/ui/spinBackground.png);\n"
"	background-repeat: no-repeat;\n"
"	background-position: right bottom;\n"
"}\n"
"\n"
"QSpinBox:focus {\n"
"	color: #FFFFFF;\n"
"}\n"
"\n"
"QSpinBox::up-button {\n"
"    subcontrol-origin: border;\n"
"    subcontrol-position: top right; /* position at the top right corner */\n"
"    width: 18px;\n"
"    border-image: url(:/ui/spinUpReleased.png);\n"
"}\n"
"\n"
"QSpinBox::up-button:hover {\n"
"    border-image: url(:/ui/spinUpHovered.png);\n"
"}\n"
"\n"
"QSpinBox::up-button:pressed {\n"
"    border-image: url(:/ui/spinUpPressed.png);\n"
"}\n"
"\n"
"QSpinBox::down-button {\n"
"    subcontrol-origin: border;\n"
"    subcontrol-position: bottom right; /* position at the top right corner */\n"
"    width: 18px;\n"
"    bo"
                        "rder-image: url(:/ui/spinDownReleased.png);\n"
"}\n"
"\n"
"QSpinBox::down-button:hover {\n"
"    border-image: url(:/ui/spinDownHovered.png);\n"
"}\n"
"\n"
"QSpinBox::down-button:pressed {\n"
"    border-image: url(:/ui/spinDownPressed.png);\n"
"}\n"
"\n"
"#low_res_view {\n"
"	border-style: none;\n"
"	background-color: #222222;\n"
"}\n"
"\n"
"#full_res_view {\n"
"	background-color: #222222;\n"
"}\n"
"\n"
"#fullResFrame {\n"
"	border-color: #007FFF;\n"
"	background-color: #222222;\n"
"}\n"
""));
        actionAbout = new QAction(FaDummyIT_GUIClass);
        actionAbout->setObjectName(QStringLiteral("actionAbout"));
        centralWidget = new QWidget(FaDummyIT_GUIClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setStyleSheet(QStringLiteral("border-color: #FF0000;"));
        low_res_view = new Low_Resolution_View(centralWidget);
        low_res_view->setObjectName(QStringLiteral("low_res_view"));
        low_res_view->setGeometry(QRect(676, 48, 420, 280));
        low_res_view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        low_res_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        comboBox = new QComboBox(centralWidget);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(676, 543, 151, 36));
        QFont font;
        font.setFamily(QStringLiteral("Lato"));
        font.setBold(true);
        font.setWeight(75);
        comboBox->setFont(font);
        slider_2 = new QSlider(centralWidget);
        slider_2->setObjectName(QStringLiteral("slider_2"));
        slider_2->setGeometry(QRect(672, 454, 304, 22));
        slider_2->setMinimum(1);
        slider_2->setMaximum(20);
        slider_2->setPageStep(3);
        slider_2->setValue(1);
        slider_2->setTracking(false);
        slider_2->setOrientation(Qt::Horizontal);
        radius_slider = new QSlider(centralWidget);
        radius_slider->setObjectName(QStringLiteral("radius_slider"));
        radius_slider->setGeometry(QRect(672, 400, 304, 22));
        radius_slider->setMinimum(1);
        radius_slider->setMaximum(20);
        radius_slider->setPageStep(3);
        radius_slider->setValue(1);
        radius_slider->setTracking(false);
        radius_slider->setOrientation(Qt::Horizontal);
        radius_slider_label = new QLabel(centralWidget);
        radius_slider_label->setObjectName(QStringLiteral("radius_slider_label"));
        radius_slider_label->setGeometry(QRect(676, 370, 61, 16));
        radius_slider_label->setFont(font);
        radius_slider_label->setStyleSheet(QLatin1String("font-family: Lato;\n"
"font-weight: bold;\n"
"font-size: 12px;\n"
"color: #798387;"));
        slider2_spin_box = new QSpinBox(centralWidget);
        slider2_spin_box->setObjectName(QStringLiteral("slider2_spin_box"));
        slider2_spin_box->setGeometry(QRect(1020, 440, 75, 28));
        QFont font1;
        font1.setFamily(QStringLiteral("Lato"));
        slider2_spin_box->setFont(font1);
        slider2_spin_box->setCursor(QCursor(Qt::ArrowCursor));
        slider2_spin_box->setMinimum(1);
        slider2_spin_box->setMaximum(20);
        slider2_spin_box->setValue(1);
        checkBox = new QCheckBox(centralWidget);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setGeometry(QRect(676, 500, 201, 20));
        checkBox->setFont(font);
        radius_spin_box = new QSpinBox(centralWidget);
        radius_spin_box->setObjectName(QStringLiteral("radius_spin_box"));
        radius_spin_box->setGeometry(QRect(1020, 386, 75, 28));
        radius_spin_box->setFont(font1);
        radius_spin_box->setMinimum(1);
        radius_spin_box->setMaximum(20);
        radius_spin_box->setValue(1);
        slider_2_label = new QLabel(centralWidget);
        slider_2_label->setObjectName(QStringLiteral("slider_2_label"));
        slider_2_label->setGeometry(QRect(676, 430, 57, 16));
        slider_2_label->setFont(font);
        slider_2_label->setStyleSheet(QLatin1String("font-family: Lato;\n"
"font-weight: bold;\n"
"font-size: 12px;\n"
"color: #798387;"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(16, 14, 115, 19));
        label->setFont(font1);
        label->setStyleSheet(QLatin1String("font-family: Lato;\n"
"font-size: 16px;\n"
"color: #FFFFFF;"));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(140, 16, 61, 17));
        label_2->setFont(font);
        label_2->setStyleSheet(QLatin1String("font-family: Lato;\n"
"font-weight: bold;\n"
"font-size: 14px;\n"
"color: #545454;"));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(836, 16, 101, 17));
        label_3->setFont(font);
        label_3->setStyleSheet(QLatin1String("font-family: Lato;\n"
"font-weight: bold;\n"
"font-size: 14px;\n"
"color: #FFFFFF;"));
        label_3->setAlignment(Qt::AlignCenter);
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(770, 340, 191, 20));
        label_4->setFont(font);
        label_4->setStyleSheet(QLatin1String("font-family: Lato;\n"
"font-weight: bold;\n"
"font-size: 14px;\n"
"color: #FFFFFF;"));
        label_4->setAlignment(Qt::AlignCenter);
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(130, 16, 16, 16));
        label_5->setStyleSheet(QLatin1String("font-family: Lato Bold;\n"
"font-size: 20px;\n"
"color: #545454;"));
        apply_button = new QPushButton(centralWidget);
        apply_button->setObjectName(QStringLiteral("apply_button"));
        apply_button->setGeometry(QRect(1000, 650, 100, 40));
        QPalette palette1;
        QBrush brush8(QColor(255, 255, 255, 255));
        brush8.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush8);
        QBrush brush9(QColor(34, 34, 34, 255));
        brush9.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush9);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush8);
        palette1.setBrush(QPalette::Active, QPalette::BrightText, brush8);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush8);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush9);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush9);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush8);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush9);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush8);
        palette1.setBrush(QPalette::Inactive, QPalette::BrightText, brush8);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush8);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush9);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush9);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush8);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush9);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush8);
        palette1.setBrush(QPalette::Disabled, QPalette::BrightText, brush8);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush8);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush9);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush9);
        apply_button->setPalette(palette1);
        apply_button->setFont(font);
        QIcon icon;
        icon.addFile(QStringLiteral(":/ui/apply.png"), QSize(), QIcon::Normal, QIcon::Off);
        apply_button->setIcon(icon);
        cancel_button = new QPushButton(centralWidget);
        cancel_button->setObjectName(QStringLiteral("cancel_button"));
        cancel_button->setGeometry(QRect(890, 650, 100, 40));
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::WindowText, brush8);
        palette2.setBrush(QPalette::Active, QPalette::Button, brush9);
        palette2.setBrush(QPalette::Active, QPalette::Text, brush8);
        palette2.setBrush(QPalette::Active, QPalette::BrightText, brush8);
        palette2.setBrush(QPalette::Active, QPalette::ButtonText, brush8);
        palette2.setBrush(QPalette::Active, QPalette::Base, brush9);
        palette2.setBrush(QPalette::Active, QPalette::Window, brush9);
        palette2.setBrush(QPalette::Inactive, QPalette::WindowText, brush8);
        palette2.setBrush(QPalette::Inactive, QPalette::Button, brush9);
        palette2.setBrush(QPalette::Inactive, QPalette::Text, brush8);
        palette2.setBrush(QPalette::Inactive, QPalette::BrightText, brush8);
        palette2.setBrush(QPalette::Inactive, QPalette::ButtonText, brush8);
        palette2.setBrush(QPalette::Inactive, QPalette::Base, brush9);
        palette2.setBrush(QPalette::Inactive, QPalette::Window, brush9);
        palette2.setBrush(QPalette::Disabled, QPalette::WindowText, brush8);
        palette2.setBrush(QPalette::Disabled, QPalette::Button, brush9);
        palette2.setBrush(QPalette::Disabled, QPalette::Text, brush8);
        palette2.setBrush(QPalette::Disabled, QPalette::BrightText, brush8);
        palette2.setBrush(QPalette::Disabled, QPalette::ButtonText, brush8);
        palette2.setBrush(QPalette::Disabled, QPalette::Base, brush9);
        palette2.setBrush(QPalette::Disabled, QPalette::Window, brush9);
        cancel_button->setPalette(palette2);
        cancel_button->setFont(font);
        about_button = new QPushButton(centralWidget);
        about_button->setObjectName(QStringLiteral("about_button"));
        about_button->setGeometry(QRect(676, 650, 100, 40));
        QPalette palette3;
        palette3.setBrush(QPalette::Active, QPalette::WindowText, brush8);
        palette3.setBrush(QPalette::Active, QPalette::Button, brush9);
        palette3.setBrush(QPalette::Active, QPalette::Text, brush8);
        palette3.setBrush(QPalette::Active, QPalette::BrightText, brush8);
        palette3.setBrush(QPalette::Active, QPalette::ButtonText, brush8);
        palette3.setBrush(QPalette::Active, QPalette::Base, brush9);
        palette3.setBrush(QPalette::Active, QPalette::Window, brush9);
        palette3.setBrush(QPalette::Inactive, QPalette::WindowText, brush8);
        palette3.setBrush(QPalette::Inactive, QPalette::Button, brush9);
        palette3.setBrush(QPalette::Inactive, QPalette::Text, brush8);
        palette3.setBrush(QPalette::Inactive, QPalette::BrightText, brush8);
        palette3.setBrush(QPalette::Inactive, QPalette::ButtonText, brush8);
        palette3.setBrush(QPalette::Inactive, QPalette::Base, brush9);
        palette3.setBrush(QPalette::Inactive, QPalette::Window, brush9);
        palette3.setBrush(QPalette::Disabled, QPalette::WindowText, brush8);
        palette3.setBrush(QPalette::Disabled, QPalette::Button, brush9);
        palette3.setBrush(QPalette::Disabled, QPalette::Text, brush8);
        palette3.setBrush(QPalette::Disabled, QPalette::BrightText, brush8);
        palette3.setBrush(QPalette::Disabled, QPalette::ButtonText, brush8);
        palette3.setBrush(QPalette::Disabled, QPalette::Base, brush9);
        palette3.setBrush(QPalette::Disabled, QPalette::Window, brush9);
        about_button->setPalette(palette3);
        about_button->setFont(font);
        fullResFrame = new QFrame(centralWidget);
        fullResFrame->setObjectName(QStringLiteral("fullResFrame"));
        fullResFrame->setGeometry(QRect(16, 48, 640, 640));
        fullResFrame->setFrameShape(QFrame::StyledPanel);
        fullResFrame->setFrameShadow(QFrame::Raised);
        full_res_view = new Full_Resolution_View(fullResFrame);
        full_res_view->setObjectName(QStringLiteral("full_res_view"));
        full_res_view->setGeometry(QRect(0, 0, 640, 640));
        full_res_view->viewport()->setProperty("cursor", QVariant(QCursor(Qt::ArrowCursor)));
        full_res_view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        full_res_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        full_res_view->setSceneRect(QRectF(0, 0, 0, 0));
        full_res_view->setDragMode(QGraphicsView::ScrollHandDrag);
        loader_widget = new loader(fullResFrame);
        loader_widget->setObjectName(QStringLiteral("loader_widget"));
        loader_widget->setGeometry(QRect(0, 0, 640, 640));
        loader_widget->setAutoFillBackground(false);
        loader_widget->setStyleSheet(QStringLiteral(""));
        dial = new QDial(centralWidget);
        dial->setObjectName(QStringLiteral("dial"));
        dial->setGeometry(QRect(870, 493, 71, 71));
        FaDummyIT_GUIClass->setCentralWidget(centralWidget);

        retranslateUi(FaDummyIT_GUIClass);
        QObject::connect(radius_slider, SIGNAL(valueChanged(int)), FaDummyIT_GUIClass, SLOT(Radius_Silder_Changed(int)));
        QObject::connect(apply_button, SIGNAL(released()), FaDummyIT_GUIClass, SLOT(Apply_Button()));
        QObject::connect(radius_slider, SIGNAL(valueChanged(int)), radius_spin_box, SLOT(setValue(int)));
        QObject::connect(radius_spin_box, SIGNAL(valueChanged(int)), radius_slider, SLOT(setValue(int)));
        QObject::connect(about_button, SIGNAL(released()), FaDummyIT_GUIClass, SLOT(About()));
        QObject::connect(cancel_button, SIGNAL(released()), FaDummyIT_GUIClass, SLOT(Cancel_Button()));
        QObject::connect(slider_2, SIGNAL(valueChanged(int)), slider2_spin_box, SLOT(setValue(int)));
        QObject::connect(slider2_spin_box, SIGNAL(valueChanged(int)), slider_2, SLOT(setValue(int)));
        QObject::connect(slider_2, SIGNAL(valueChanged(int)), FaDummyIT_GUIClass, SLOT(Silder2_Changed(int)));
        QObject::connect(checkBox, SIGNAL(stateChanged(int)), FaDummyIT_GUIClass, SLOT(Check_Box_Changed(int)));
        QObject::connect(comboBox, SIGNAL(currentIndexChanged(int)), FaDummyIT_GUIClass, SLOT(Drop_Box_Change(int)));

        QMetaObject::connectSlotsByName(FaDummyIT_GUIClass);
    } // setupUi

    void retranslateUi(QMainWindow *FaDummyIT_GUIClass)
    {
        FaDummyIT_GUIClass->setWindowTitle(QApplication::translate("FaDummyIT_GUIClass", "FaDummyIT_GUI", 0));
        actionAbout->setText(QApplication::translate("FaDummyIT_GUIClass", "About", 0));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("FaDummyIT_GUIClass", "DROP OPTION 1", 0)
         << QApplication::translate("FaDummyIT_GUIClass", "DROP OPTION 2", 0)
        );
        radius_slider_label->setText(QApplication::translate("FaDummyIT_GUIClass", "RADIUS", 0));
        checkBox->setText(QApplication::translate("FaDummyIT_GUIClass", "CHECKBOX", 0));
        slider_2_label->setText(QApplication::translate("FaDummyIT_GUIClass", "SLIDER 2", 0));
        label->setText(QApplication::translate("FaDummyIT_GUIClass", "Fixel Algorithms", 0));
        label_2->setText(QApplication::translate("FaDummyIT_GUIClass", "DUMMY", 0));
        label_3->setText(QApplication::translate("FaDummyIT_GUIClass", "NAVIGATION", 0));
        label_4->setText(QApplication::translate("FaDummyIT_GUIClass", "PARAMETERS PANEL", 0));
        label_5->setText(QApplication::translate("FaDummyIT_GUIClass", "|", 0));
        apply_button->setText(QApplication::translate("FaDummyIT_GUIClass", "Apply", 0));
        cancel_button->setText(QApplication::translate("FaDummyIT_GUIClass", "Cancel", 0));
        about_button->setText(QApplication::translate("FaDummyIT_GUIClass", "About", 0));
    } // retranslateUi

};

namespace Ui {
    class FaDummyIT_GUIClass: public Ui_FaDummyIT_GUIClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FADUMMYIT_GUI_H
