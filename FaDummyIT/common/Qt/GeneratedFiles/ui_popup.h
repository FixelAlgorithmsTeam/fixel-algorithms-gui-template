/********************************************************************************
** Form generated from reading UI file 'popup.ui'
**
** Created by: Qt User Interface Compiler version 5.5.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_POPUP_H
#define UI_POPUP_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_popup
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QLabel *label_5;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_2;
    QLabel *content_label;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *close_button;

    void setupUi(QDialog *popup)
    {
        if (popup->objectName().isEmpty())
            popup->setObjectName(QStringLiteral("popup"));
        popup->resize(480, 445);
        popup->setMinimumSize(QSize(480, 0));
        popup->setMaximumSize(QSize(480, 16777215));
        popup->setStyleSheet(QLatin1String("#popup {\n"
"	background-color: #171616;\n"
"}\n"
"\n"
"QPushButton {\n"
"	background-color: #222222;\n"
"	border-radius: 5px;\n"
"	border-style: none;\n"
"	min-height: 40px;\n"
"	max-height: 40px;\n"
"	min-width: 100px;\n"
"	font-family: Lato;\n"
"	font-size: 14px;\n"
"	color: #FFFFFF;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"	background-color: #333333;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"	background-color: #444444;\n"
"}\n"
"\n"
"#content_label {\n"
"  font-family: Arial;\n"
"  font-size: 14px;\n"
"  color: #FFFFFF;\n"
"}"));
        verticalLayout = new QVBoxLayout(popup);
        verticalLayout->setSpacing(16);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(16, 16, 16, 16);
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label = new QLabel(popup);
        label->setObjectName(QStringLiteral("label"));
        QFont font;
        font.setFamily(QStringLiteral("Lato"));
        label->setFont(font);
        label->setStyleSheet(QLatin1String("font-family: Lato;\n"
"font-size: 16px;\n"
"color: #FFFFFF;"));

        horizontalLayout_2->addWidget(label);

        label_5 = new QLabel(popup);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setStyleSheet(QLatin1String("font-family: Lato Bold;\n"
"font-size: 20px;\n"
"color: #545454;"));

        horizontalLayout_2->addWidget(label_5);

        label_2 = new QLabel(popup);
        label_2->setObjectName(QStringLiteral("label_2"));
        QFont font1;
        font1.setFamily(QStringLiteral("Lato"));
        font1.setBold(true);
        font1.setWeight(75);
        label_2->setFont(font1);
        label_2->setStyleSheet(QLatin1String("font-family: Lato;\n"
"font-weight: bold;\n"
"font-size: 14px;\n"
"color: #545454;"));

        horizontalLayout_2->addWidget(label_2);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout_2);

        content_label = new QLabel(popup);
        content_label->setObjectName(QStringLiteral("content_label"));
        content_label->setMinimumSize(QSize(448, 0));
        content_label->setMaximumSize(QSize(448, 16777215));
        content_label->setScaledContents(true);
        content_label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        content_label->setWordWrap(true);

        verticalLayout->addWidget(content_label);

        verticalSpacer = new QSpacerItem(20, 134, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        close_button = new QPushButton(popup);
        close_button->setObjectName(QStringLiteral("close_button"));

        horizontalLayout->addWidget(close_button);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(popup);

        QMetaObject::connectSlotsByName(popup);
    } // setupUi

    void retranslateUi(QDialog *popup)
    {
        popup->setWindowTitle(QApplication::translate("popup", "Dialog", 0));
        label->setText(QApplication::translate("popup", "Fixel Algorithms", 0));
        label_5->setText(QApplication::translate("popup", "|", 0));
        label_2->setText(QApplication::translate("popup", "ABOUT", 0));
        content_label->setText(QApplication::translate("popup", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eleifend euismod enim laoreet vestibulum. Nulla eget consectetur felis. Morbi mauris lacus, viverra sed ligula eget, pellentesque tincidunt odio. Donec vulputate viverra orci, ut imperdiet lacus ultricies at. Curabitur molestie fringilla ex egestas auctor. Vestibulum viverra non massa tempus venenatis. Praesent eget eleifend neque, eget accumsan nisl. Ut sagittis eleifend fringilla. Nulla id accumsan eros, eu vehicula diam. In sodales sem id purus venenatis tempor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Quisque interdum non arcu laoreet faucibus.", 0));
        close_button->setText(QApplication::translate("popup", "Close", 0));
    } // retranslateUi

};

namespace Ui {
    class popup: public Ui_popup {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_POPUP_H
