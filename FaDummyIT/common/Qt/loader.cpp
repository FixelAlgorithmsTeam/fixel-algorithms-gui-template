#include "loader.h"
#include "ui_loader.h"
#include <QApplication>
#include <QCursor>
//#include <QGraphicsOpacityEffect>
//#include <QPalette>

loader::loader(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::loader)
{
  ui->setupUi(this);
  setAutoFillBackground(true);

  //setWindowOpacity(0.5);

  //QPalette pal(palette());
  //pal.setColor(QPalette::Background, Qt::black);
  //setPalette(pal);

  //QGraphicsOpacityEffect *effect = new QGraphicsOpacityEffect(this);
  //effect->setOpacity(0.5);
  //effect->setOpacityMask(QBrush(QColor("#000000")));
  //setGraphicsEffect(effect);

  m_pMovie = new QMovie(":/ui/loaderAnimation.gif", QByteArray(), this);
  ui->loader_label->setMovie(m_pMovie);
  setVisible(false);
}

loader::~loader() {
  delete ui;
}

void loader::start() {
  m_pMovie->start();
  setVisible(true);
  qApp->setOverrideCursor(QCursor(Qt::WaitCursor));
}

void loader::stop() {
  m_pMovie->stop();
  setVisible(false);
  qApp->setOverrideCursor(QCursor(Qt::ArrowCursor));
}
