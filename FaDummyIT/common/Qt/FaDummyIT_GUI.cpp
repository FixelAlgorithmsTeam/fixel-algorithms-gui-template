#include "FaDummyIT_GUI.h"

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QListView>

#include "FaDummyIT_Filter.h"

FaDummyIT_GUI::FaDummyIT_GUI(FaImage& rhs_image, QWidget *parent):
	ui         (),
    input_image(rhs_image),
	QMainWindow(parent)
{
	ui.setupUi(this);
	pPopup = new popup(this);

	//test stuff that has to be removed/changed in release version
	ui.loader_widget->start(); 
	//ui.loader_label->stop(); 
	//end test stuff

	ui.comboBox->setView(new QListView(ui.comboBox));
	int view_width  = qMin(ui.full_res_view->width(),  input_image.Width());
	int view_height = qMin(ui.full_res_view->height(), input_image.Height());;

	ui.full_res_view->setGeometry(QRect(0, 0, view_width, view_height));

	this->image_width  = ui.full_res_view->width() - 2;
	this->image_height = ui.full_res_view->height() - 2;

	pOutput_image = new FaImage(image_width, image_height, 3);

	this->pQPreview_image = new QImage(this->image_width,
		                               this->image_height, QImage::Format_RGB32);

	this->pQInput_image   = new QImage(input_image.Width(), input_image.Height(), QImage::Format_RGB32);
	for (int i = 0; i < input_image.Width(); i++)
	{
		for (int j = 0; j < input_image.Height(); j++)
		{
			QRgb pixel = qRgb(255 * input_image.At(0, i, j),
				              255 * input_image.At(1, i, j),
							  255 * input_image.At(2, i, j) );
			this->pQInput_image->setPixel(i, j, pixel);
		}
	}
}

FaDummyIT_GUI::~FaDummyIT_GUI()
{
	delete this->pOutput_image;

	delete this->pQInput_image;
	delete this->pQPreview_image;
}

void FaDummyIT_GUI::Init(FaDummyAlgorithmParams* pAlgorithm_params_rhs)
{
	this->pAlgorithm_params = pAlgorithm_params_rhs;

	// General::
	sWrapper_params.apply_filter = false;

	// ROI:
	sWrapper_params.roi_x      = 0;
	sWrapper_params.roi_y      = 0;
	sWrapper_params.roi_width  = this->input_image.Width();
	sWrapper_params.roi_height = this->input_image.Height();

	// GUI::
	sWrapper_params.radius = 1;


	this->ui.full_res_view->Init();
	this->ui.low_res_view->Init();

	this->Render();
	this->Display_Preview();
}

void FaDummyIT_GUI::Render()
{
	sWrapper_params.pOutput_image = pOutput_image;
	sWrapper_params.pInput_image  = &this->input_image;
	sWrapper_params.roi_x         = qMax<int>(0, this->qPoint.x());
	sWrapper_params.roi_y         = qMax<int>(0, this->qPoint.y());
	sWrapper_params.roi_width     = this->image_width;
	sWrapper_params.roi_height    = this->image_height;

	for (int i = 0; i < 1; i++)
		FaDummyWrapper(sWrapper_params, *this->pAlgorithm_params);
}

void FaDummyIT_GUI::Display_Preview()
{
	QGraphicsScene*&      pPreview_scene         = this->ui.full_res_view->pPreview_scene;
	QGraphicsPixmapItem*& pFull_res_preview_item = this->ui.full_res_view->pFull_res_preview_item;

	for (int i = 0; i < pOutput_image->Width(); i++)
	{
		for (int j = 0; j < pOutput_image->Height(); j++)
		{
			QRgb pixel = qRgb(255 * pOutput_image->At(0, i, j),
							  255 * pOutput_image->At(1, i, j),
							  255 * pOutput_image->At(2, i, j));
			pQPreview_image->setPixel(i, j, pixel);
		}
	}
	pFull_res_preview_item->setPixmap(QPixmap::fromImage(*pQPreview_image));
	pFull_res_preview_item->setPos(this->qPoint);
	pPreview_scene->addItem(pFull_res_preview_item);
}

void FaDummyIT_GUI::Radius_Silder_Changed(int radius_silder_value)
{
	sWrapper_params.radius = radius_silder_value;
	this->Render();
	this->Display_Preview();
}

void FaDummyIT_GUI::Silder2_Changed(int slider2_value)
{
	sWrapper_params.slider2 = slider2_value;
}

void FaDummyIT_GUI::Check_Box_Changed(int check_box)
{
	sWrapper_params.check_box = check_box;
}

void FaDummyIT_GUI::Drop_Box_Change(int drop_box_value)
{
	sWrapper_params.drop_box = drop_box_value;
}

void FaDummyIT_GUI::Apply_Button()
{
	sWrapper_params.apply_filter = true;
	this->close();
}

void FaDummyIT_GUI::Cancel_Button()
{
	sWrapper_params.apply_filter = false;
	this->close();
}

void FaDummyIT_GUI::About()
{
	pPopup->setContentText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
		"Nullam eleifend euismod enim laoreet vestibulum. Nulla "
		"eget consectetur felis. Morbi mauris lacus, viverra sed "
		"ligula eget, pellentesque tincidunt odio. Donec vulputate "
		"viverra orci, ut imperdiet lacus ultricies at. Curabitur "
		"molestie fringilla ex egestas auctor. Vestibulum viverra "
		"non massa tempus venenatis. Praesent eget eleifend neque, "
		"eget accumsan nisl. Ut sagittis eleifend fringilla. Nulla "
		"id accumsan eros, eu vehicula diam. In sodales sem id purus "
		"venenatis tempor. Vestibulum ante ipsum primis in faucibus "
		"orci luctus et ultrices posuere cubilia Curae; Quisque "
		"interdum non arcu laoreet faucibus.");

	pPopup->show();
}

void Done_Button();
