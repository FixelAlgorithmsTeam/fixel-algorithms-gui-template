
#include <stdint.h>

#include "FaPS.h"

FaImage* ConvertFromPS(PS_Database& sDatabase)
{
	FaImage* pNewImage = new FaImage(sDatabase.width, sDatabase.height, sDatabase.planes);
	FaImage&  newImage = *pNewImage;

	switch (sDatabase.format)
	{
	case PS_Database::FORMAT_8:  ConvertInImage<uint8_t> (newImage, sDatabase); break;
	case PS_Database::FORMAT_16: ConvertInImage<uint16_t>(newImage, sDatabase); break;
	case PS_Database::FORMAT_32: ConvertInImage<float>   (newImage, sDatabase); break;
	}
	return pNewImage;
}

void ConvertToPS(const FaImage& sourceImage, PS_Database& sDatabase)
{
	switch (sDatabase.format)
	{
	case PS_Database::FORMAT_8:  ConvertOutImage<uint8_t> (sourceImage, sDatabase); break;
	case PS_Database::FORMAT_16: ConvertOutImage<uint16_t>(sourceImage, sDatabase); break;
	case PS_Database::FORMAT_32: ConvertOutImage<float>   (sourceImage, sDatabase); break;
	}
}
