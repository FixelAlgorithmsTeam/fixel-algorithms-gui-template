
#include "FaImage.h"
#include <cmath>

static int n = 0;
void ImageMalloc( q32f** pLayer, int* pStep, int planes, int width, int height)
{
	for( int plane = 0; plane < planes; ++plane )
	{
		pLayer[plane] = new q32f[width * height];
		pStep[plane] = width * sizeof(q32f);
		n++;
		if( pLayer[plane] == NULL )
			throw NO_MEMORY_ERROR();
	}
}

void ImageMalloc( FaImage& image )
{
	ImageMalloc(image.layer_, image.bytes_step_, image.planes_, image.width_, image.height_);
}

/*------Constructors------------------------------------------------------*/

FaImage::FaImage():
    planes_    (0),
	layer_     (NULL),
	bytes_step_(NULL),
	type_      (NONE)
{}

FaImage::FaImage(int width, int height, int planes):
	planes_    (planes),
	layer_     (new q32f*[planes_]),
	bytes_step_(new int[planes_]),
	type_      (NONE)
{
	width_  = width;
	height_ = height;

	ImageMalloc(layer_, bytes_step_, planes_, width_, height_);
}

FaImage::FaImage(FaImage* pRhsImage):
    planes_    (0),
	layer_     (NULL),
	bytes_step_(NULL),
	type_      (NONE)
{
	operator=(pRhsImage);
}

void FaImage::operator = (FaImage* pRhsImage)
{
	if (this == pRhsImage)
		return;

	this->type_   = pRhsImage->type_;
	width_        = pRhsImage->width_;
	height_       = pRhsImage->height_;

	if (planes_ == 0 || planes_ < pRhsImage->planes_)
	{
		planes_ = pRhsImage->planes_;
		delete[] layer_;
		layer_  = pRhsImage->layer_;
		delete bytes_step_;
		bytes_step_   = pRhsImage->bytes_step_;
	}
	else
	{
		for (int plane = 0; plane < planes_; ++plane)
		{
			delete[] layer_[plane];
			n--;
			layer_[plane] = pRhsImage->layer_[plane];
			bytes_step_[plane]  = pRhsImage->bytes_step_[plane];
		}
	}
}

FaImage::~FaImage()
{
	switch (this->type_)
	{
	case NONE:
		for (int plane = 0; plane < planes_; ++plane)
		{
			delete[] layer_[plane];
			n--;
		}
		delete[] layer_;
		delete[] bytes_step_;
		break;

	case REF_IMAGE: break;
	case PAD_IMAGE:
	case SUB_IMAGE:
		delete[] this->layer_;
		break;

	case MATLAB:
		delete[] this->layer_;
		delete   this->bytes_step_;
		break;

	}
}

/*-----------------------------------------------------------------------------------*/

static FaImage ROI_Aux(const FaImage& rhs_Image, int x, int y, int width, int height)
{
	FaImage* pTempImage = new FaImage();
	FaImage& tempImage  = *pTempImage;

	tempImage.type_        = FaImage::SUB_IMAGE;
	tempImage.planes_      = rhs_Image.planes_;
	tempImage.width_  = width;
    tempImage.height_ = height;


	tempImage.bytes_step_  = rhs_Image.bytes_step_;
	tempImage.layer_ = new q32f*[tempImage.planes_];

	for (int plane = 0; plane < tempImage.planes_; plane++)
	{
		tempImage.layer_[plane] =
			rhs_Image.layer_[plane] + y * tempImage.Step() + x;
	}

	return tempImage;
}

const FaImage FaImage::ROI(int x, int y, int width, int height) const
{
	return ROI_Aux(*this, x, y, width, height);
}

FaImage FaImage::ROI(int x, int y, int width, int height)
{
	return ROI_Aux(*this, x, y, width, height);
}

float& FaImage::At(int c, int x, int y)
{
	return this->layer_[c][y * this->Step() + x];
}
