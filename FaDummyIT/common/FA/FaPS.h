
#ifndef _FA_PS_
#define _FA_PS_

#include "FaImage.h"

#define MAX8  255
#define MAX16 32768
#define MAX32 1

struct PS_Database
{
	void *pInputImage;
	void *pOutputImage;
	int   planes;
	int   inputStep;
	int   outputStep;
	int   width;
	int   height;

	enum { FORMAT_8, FORMAT_16, FORMAT_32 } format;
};

FaImage* ConvertFromPS(PS_Database&);
void     ConvertToPS  (const FaImage& sourceImage, PS_Database&);

template<typename T>
void ConvertInImage(FaImage& dstImage, PS_Database& sDatabase)
{
	T*  in = static_cast<T*>(sDatabase.pInputImage);

	int dstStep = dstImage.bytes_step_[0]    / sizeof(q32f);
	int srcStep = sDatabase.inputStep / sizeof(T);

	for (int y = 0; y < dstImage.Height(); ++y)
	{
		T* start = in;
		#pragma ivdep
		for (int x = 0; x < dstImage.Width(); ++x)
		{
			#pragma ivdep
			for (int plane = 0; plane < dstImage.planes_; ++plane)
			{
				dstImage.layer_[plane][y * dstStep + x] = static_cast<q32f>(*in++);
			}

		}
		in = start + srcStep;
	}
}


template<typename T>
void ConvertOutImage(const FaImage& sourceImage, PS_Database& sDatabase)
{
	T* out = static_cast<T*>(sDatabase.pOutputImage);

	int srcStep = sourceImage.bytes_step_[0] / sizeof(q32f);
	int dstStep = sDatabase.inputStep / sizeof(T);

	for(int y = 0; y < sourceImage.Height(); ++y)
	{
		T* start = out;

        #pragma ivdep
		for (int x = 0; x < sourceImage.Width(); ++x)
		{
			#pragma ivdep
			for (int plane = 0; plane < sourceImage.planes_; ++plane)
			{
				*out++ = static_cast<T>(sourceImage.layer_[plane][y * srcStep + x]);
			}
		}
		out = start + dstStep;
	}
}

#endif
