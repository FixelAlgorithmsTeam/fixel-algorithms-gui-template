
#ifndef FA_IMAGE_STRACTURE_H
#define FA_IMAGE_STRACTURE_H

#include <ctime>
#include <list>

typedef float q32f;

class NO_MEMORY_ERROR {};

class FaImage
{
public:
	enum image_type
	{
		NONE,
		REF_IMAGE,
		PAD_IMAGE,
		SUB_IMAGE,
		MATLAB,
	};

	int    planes_;
	q32f** layer_;
	int*   bytes_step_;
	int    width_;
	int    height_;

	image_type type_;

	/* Constructors */
	FaImage();
	FaImage(int width, int height, int planes);
	FaImage(FaImage* pRhsImage);

	virtual ~FaImage();

	void operator = (FaImage* pRhsImage);

	float& At      (int c, int x, int y);

	const FaImage ROI         (int x, int y, int width, int height)  const;
	      FaImage ROI         (int x, int y, int width, int height);


	int  Width       () const { return width_;  }
	int  Height      () const { return height_; }
	int  ChannelsNum () const { return planes_; }
	int  Step        () const { return bytes_step_[0] / sizeof(q32f); }
};

void ImageMalloc( q32f** pLayer, int* pStep, int planes, int width, int height);
void ImageMalloc( FaImage& image );

#endif
