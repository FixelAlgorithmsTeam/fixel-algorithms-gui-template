
#include "FaDummyIT_Filter.h"

struct FaDummyItWrapperParams sWrapper_params;

void FaDummyPreprocess(FaDummyAlgorithmParams& sParams)
{
	sParams.image_minimum = 1;
}

void FaDummyWrapper(FaDummyItWrapperParams& sWrapper_params, FaDummyAlgorithmParams&)
{
	FaImage input_image = sWrapper_params.pInput_image->ROI(
                              sWrapper_params.roi_x, sWrapper_params.roi_y,
							  sWrapper_params.roi_width, sWrapper_params.roi_height);
	FaImage& outputImage = *sWrapper_params.pOutput_image;

	for (int i = 0; i < outputImage.Width(); i++)
	{
		for (int j = 0; j < outputImage.Height(); j++)
		{
			for (int p = 0; p < 3; p++)
			{
				outputImage.At(p, i, j) = input_image.At(p, i, j) * sWrapper_params.radius;
			}
		}
	}
}
