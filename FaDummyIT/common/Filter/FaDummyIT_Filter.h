#ifndef _GAUSS_BLUR_IT_FILTER_H_
#define _GAUSS_BLUR_IT_FILTER_H_

#include "FaImage.h"

struct FaDummyAlgorithmParams
{
	// Preprocess:
	float image_minimum;
};

struct FaDummyItWrapperParams
{
	// General:
	FaImage* pInput_image;
	FaImage* pOutput_image;

	bool apply_filter;

	// GUI (keep with visual order):
	int  radius;
	int  slider2;
	bool check_box;
	int  drop_box;

	// ROI:
	int roi_x;
	int roi_y;
	int roi_width;
	int roi_height;
};

extern struct FaDummyItWrapperParams sWrapper_params;

void FaDummyPreprocess(FaDummyAlgorithmParams&);
void FaDummyWrapper   (FaDummyItWrapperParams&, FaDummyAlgorithmParams&);

#endif // !_GAUSS_BLUR_IT_FILTER_H_
