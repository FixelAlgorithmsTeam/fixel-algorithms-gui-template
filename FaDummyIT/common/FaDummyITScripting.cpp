// ADOBE SYSTEMS INCORPORATED
// Copyright  1993 - 2002 Adobe Systems Incorporated
// All Rights Reserved
//
// NOTICE:  Adobe permits you to use, modify, and distribute this
// file in accordance with the terms of the Adobe license agreement
// accompanying it.  If you have received this file from a source
// other than Adobe, then your use, modification, or distribution
// of it requires the prior written permission of Adobe.
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//
//	File:
//		FaDummyITScripting.cpp
//
//	Description:
//		This file contains the source and routines for the
//		Filter module Poor Man's Type Tool, a module that
//		uses the Channel Ports Suite for pixel munging.
//
//	Use:
//		This is a basic module to exemplify all the typical
//		functions a filter module will do: Read scripting
//		parameters, show a user interface, display a proxy,
//		write changed pixel information, and then write
//		scripting parameters.
//
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//	Includes
//-------------------------------------------------------------------------------

#include "FaDummyIT_Plugin.h"
#include "PITerminology.h"
#include "PIStringTerminology.h"


//-------------------------------------------------------------------------------
//
//	ReadScriptParams
//
//	Checks the parameters against scripting-returned parameters, if any, and
//	updates the globals to match ones provided by the scripting system.
//
//	Inputs:
//		GPtr globals		Pointer to global structure.
//
//	Outputs:
//		gQueryForParameters = TRUE		If you should pop your dialog.
//		gQueryForParameters = FALSE		If you should not pop your dialog.
//		gQueryForParameters = unchanged	If descriptor suite is unavailable.
//
//		returns gResult = OSErr			Will return any fatal error.
//
//-------------------------------------------------------------------------------

OSErr ReadScriptParams (GPtr globals)
{
	PIReadDescriptor	token = NULL;	// token we'll use to read descriptor
	DescriptorKeyID		key = NULLID;	// the next key
	DescriptorTypeID	type = NULLID;	// the type of the key we read
	int32				flags = 0;		// any flags for the key

	// Populate this array if we're expecting any keys,
	// must be NULLID terminated:
	DescriptorKeyIDArray	array = { keyLuminosity, keyScale1, keyScale2, keyScale3,\
		                              keyScale4, keyScale5,  keyIntensityLvl, NULLID };

	// While we're reading keys, errors will stick around and be reported
	// when we close the token:
	OSErr				err = noErr;

	// Assume we want to pop our dialog unless explicitly told not to:
	Boolean				returnValue = gQueryForParameters;

	if (DescriptorAvailable(NULL))
	{ // descriptor suite is available, go ahead and open descriptor:

		// PIUtilities routine to open descriptor handed to us by host:
		token = OpenReader(array);

		if (token)
		{ // token was valid, so read keys from it:
			while (PIGetKey(token, &key, &type, &flags))
			{ // got a valid key.  Figure out where to put it:

				switch (key)
				{ // match a key to its expected type:case keyAmount:
					case keyLuminosity:
						err = PIGetBool(token, &gLumiModeFlag);
						break;
					case keyScale1:
						real64 scale1;
						err     = PIGetFloat(token, &scale1);
						gScale1 = static_cast<float>(scale1);
						break;
					case keyScale2:
						real64 scale2;
						err     = PIGetFloat(token, &scale2);
						gScale2 = static_cast<float>(scale2);
						break;
					case keyScale3:
						real64 scale3;
						err     = PIGetFloat(token, &scale3);
						gScale3 = static_cast<float>(scale3);
						break;
					case keyScale4:
						real64 scale4;
						err     = PIGetFloat(token, &scale4);
						gScale4 = static_cast<float>(scale4);
						break;
					case keyScale5:
						real64 scale5;
						err     = PIGetFloat(token, &scale5);
						gScale5 = static_cast<float>(scale5);
						break;
					case keyIntensityLvl:
						real64 intensityInput;
						err           = PIGetFloat(token, &intensityInput);
						gIntensityInput = static_cast<float>(intensityInput);
						break;
			        // ignore all other cases and classes
				} // key

			} // PIGetKey

			// PIUtilities routine that automatically deallocates,
			// closes, and sets token to NULL:
			err = CloseReader(&token);

			if (err)
			{ // an error did occur while we were reading keys:

				if (err == errMissingParameter) // missedParamErr == -1715
				{ // missing parameter somewhere.  Walk IDarray to find which one.
				}
				else
				{ // serious error.  Return it as a global result:
					gResult = err;
				}

			} // stickyError

		} // didn't have a valid token

		// Whether we had a valid token or not, we were given information
		// as to whether to pop our dialog or not.  PIUtilities has a routine
		// to check that and return TRUE if we should pop it, FALSE if not:
		returnValue = PlayDialog();

	} // descriptor suite unavailable

	gQueryForParameters = returnValue;

	return err;

} // end ReadScriptParams

//-------------------------------------------------------------------------------
//
//	WriteScriptParams
//
//	Takes our parameters from our global variables and writes them out to the
//	scripting system, which hands it all back to the host.
//
//	Inputs:
//		GPtr globals				Pointer to global structure.
//
//		short gPointH				Horizontal position of Hello World
//		short gPointV				Vertical position of Hello World
//		int32 gXFactor				Kinda like the font size
//
//		Boolean gIgnoreSelection	TRUE = Effect entire image.
//									FALSE = Effect selection only.
//
//	Outputs:
//		returns an OSErr		If there was a serious error.
//		returns noErr			If everything worked fine.
//
//-------------------------------------------------------------------------------

OSErr WriteScriptParams (GPtr globals)
{
	PIWriteDescriptor	token = NULL;	// token to write our parameters to
	OSErr				err = noErr; // we'll return any error with this

	if (DescriptorAvailable(NULL))
	{ // Descriptor suite is available.  Open a token:

		// PIUtilities has a routine that will create a descriptor to write
		// parameters to:
		token = OpenWriter();

		if (token)
		{ // was able to create the token, write our keys:

			PIPutBool (token, keyLuminosity,   gLumiModeFlag);
			PIPutFloat(token, keyIntensityLvl, &gIntensityInput);
			PIPutFloat(token, keyScale1,       &gScale1);
			PIPutFloat(token, keyScale2,       &gScale2);
			PIPutFloat(token, keyScale3,       &gScale3);
			PIPutFloat(token, keyScale4,       &gScale4);
			PIPutFloat(token, keyScale5,       &gScale5);

			// PIUtilities has a routine that will close the token, collapse
			// it, and store it in the struct that the host will then grab
			// and store for recording and/or subsequent scripting.  It tells
			// the host that the dialog is optional.  You can override this
			// by setting recordInfo to whatever you need.  It then
			// deallocates token and sets it to NULL:
			err = CloseWriter(&token);

		} // wasn't able to create token

	} // descriptor suite unavailable

	return err;

} // end WriteScriptParams

//-------------------------------------------------------------------------------
// end FaDummyITScripting.cpp
