
#include "FaDummyIT_Filter.h"
#include "FaPS.h"
#include <QtWidgets/QApplication>
#include <QFontDatabase>
#include "FaDummyIT_GUI.h"

FaDummyIT_GUI* pGUI = NULL;

void FaDummyIT(PS_Database& sDatabase)
{
	int format_factor;
	switch (sDatabase.format)
	{
	case PS_Database::FORMAT_8:  format_factor = MAX8;  break;
	case PS_Database::FORMAT_16: format_factor = MAX16; break;
	case PS_Database::FORMAT_32: format_factor = MAX32; break;
	}

	sDatabase.planes   = (sDatabase.planes > 3) ? 3 : sDatabase.planes;
	FaImage inputImage = ConvertFromPS(sDatabase);

	// normalize between 0 to 1.
	if (sDatabase.format != PS_Database::FORMAT_32)
	{
		for (int i = 0; i < inputImage.Width(); i++)
		{
			for (int j = 0; j < inputImage.Height(); j++)
			{
				for (int p = 0; p < 3; p++)
					inputImage.At(p, i, j) /= format_factor;
			}
		}
	}

	FaDummyAlgorithmParams sAlgorithm_params;
	FaDummyPreprocess(sAlgorithm_params);

	int   argc    = 0;
    char *argv[1] = {"C"};
	QApplication* app = new QApplication(argc, argv);
	QFontDatabase::addApplicationFont(":/ui/Lato-Bol.ttf");
	QFontDatabase::addApplicationFont(":/ui/Lato-Reg.ttf");

	pGUI              = new FaDummyIT_GUI(inputImage);
	pGUI->show();
	pGUI->Init(&sAlgorithm_params);
    app->exec();

	delete pGUI;
	delete app;

	if (sWrapper_params.apply_filter == false)
		return;

	FaImage outputImage(inputImage.Width(), inputImage.Height(), sDatabase.planes);

	// Filter the image:
	sWrapper_params.roi_x      = 0;
	sWrapper_params.roi_y      = 0;
	sWrapper_params.roi_width  = inputImage.Width();
	sWrapper_params.roi_height = inputImage.Height();

	sWrapper_params.pOutput_image = &outputImage;

	FaDummyWrapper(sWrapper_params, sAlgorithm_params);

	// Return to correct format
	if (sDatabase.format != PS_Database::FORMAT_32)
	{
		for (int i = 0; i < outputImage.Width(); i++)
		{
			for (int j = 0; j < outputImage.Height(); j++)
			{
				for (int p = 0; p < 3; p++)
					outputImage.At(p, i, j) *= format_factor;
			}
		}
	}

	ConvertToPS(outputImage, sDatabase);
}
